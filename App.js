import React, { useState, useEffect, useMemo } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Easing } from 'react-native'
import { UserContext } from './AuthContext'
import Login from './components/Screens/Login';
import Home from './components/Screens/Home';
import Splash from './components/Screens/Splash';
import Getstarted from './components/Screens/Getstarted';
import Signup from './components/Screens/Signup';
import Forgotpassword from './components/Screens/Forgotpassword';
import Searchpage from './components/Screens/Searchpage';
import Notifications from './components/Screens/Notifications';
import Companies from './components/Screens/Companies';
import Availablejobs from './components/Screens/Availablejobs';
import Jobdescription from './components/Screens/Jobdescription';
import Appliedjobs from './components/Screens/Appliedjobs';
import Saved from './components/Screens/Saved';
import Profile from './components/Screens/Profile';
import Basicinformation from './components/Screens/Basicinformation';
import About from './components/Screens/About';
import Workexperience from './components/Screens/Workexperience';
import Education from './components/Screens/Education';
import Preference from './components/Screens/Preference';
import Settings from './components/Screens/Settings';
import Verifyotp from './components/Screens/Verifyotp';
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";
import Chnagepassword from './components/Screens/Chnagepassword';


const App = () => {

  const Stack = createStackNavigator();
  const [user, setuser] = useState(0)
  const [loading, setloading] = useState(true)

  const getLoginData = async () => {
    try {
      const value = await AsyncStorage.getItem('token')
      if (value !== null) {
        setuser(1)
        setloading(false)
      } else {
        setuser(0)
        setloading(false)
      }
    } catch (error) {
      setuser(0)
      setloading(false)
    }
  }

  useEffect(() => {
    checkPermission()
    getLoginData()

    PushNotification.configure({
      onRegister: function (token) {
        //console.log("TOKEN:", token);
      },
      onNotification: function (notification) {
        //console.log("NOTIFICATION:", notification);
      },
      onAction: function (notification) {
        //console.log("ACTION:", notification.action);
        //console.log("NOTIFICATION:", notification);
      },
      onRegistrationError: function(err) {
        //console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });

  }, [])


  const checkPermission = async () => {
    try {
      const enabled = await messaging().hasPermission();
      if (enabled) {
        getToken();
      } else {
        requestPermission();
      }
    } catch (error) {
      console.log(error)
    }
  }

  const getToken = async () => {
    try {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      if (!fcmToken) {
        fcmToken = await messaging().getToken();
        if (fcmToken) {
          await AsyncStorage.setItem('fcmToken', fcmToken);
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  const requestPermission = async () => {
      try {
        await messaging().requestPermission();
        getToken();
      } catch (error) {
        console.log('permission rejected');
      }
  }


  const authFlow = useMemo(() => ({
    signOut: async () => {
      try {
        await AsyncStorage.removeItem('token')
        setuser(0)
        setloading(false)
      } catch (error) {
        setuser(0)
        setloading(false)
      }
    },
    logIn: async () => {
      try {
        setuser(1)
        setloading(false)
      } catch (error) {

      }
    }
  }), [user])


  const openAndCloseConfig = {
    animation: 'timing',
    config: {
      duration: 16.66,
      easing: Easing.linear
    }
  }

  return (
    <UserContext.Provider value={authFlow}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            transitionSpec: {
              open: openAndCloseConfig,
              close: openAndCloseConfig
            },
          }}
        >
          {
            loading === true ? <Stack.Screen name="loading" component={Splash} />
              : user === 0 ?
                <>
                  <Stack.Screen name="getstarted" component={Getstarted} />
                  <Stack.Screen name="login" component={Login} />
                  <Stack.Screen name="signup" component={Signup} />
                  <Stack.Screen name="verifyotp" component={Verifyotp} />
                  <Stack.Screen name="forgotpassword" component={Forgotpassword} />
                  <Stack.Screen name="changepassword" component={Chnagepassword} />
                </> :
                <>
                  <Stack.Screen name="home" component={Home} />
                  <Stack.Screen name="search" component={Searchpage} />
                  <Stack.Screen name="notifications" component={Notifications} />
                  <Stack.Screen name="companies" component={Companies} />
                  <Stack.Screen name="availablejobs" component={Availablejobs} />
                  <Stack.Screen name="jobdescription" component={Jobdescription} />
                  <Stack.Screen name="appliedjobs" component={Appliedjobs} />
                  <Stack.Screen name="saved" component={Saved} />
                  <Stack.Screen name="profile" component={Profile} />
                  <Stack.Screen name="basicinformation" component={Basicinformation} />
                  <Stack.Screen name="about" component={About} />
                  <Stack.Screen name="worksexperience" component={Workexperience} />
                  <Stack.Screen name="education" component={Education} />
                  <Stack.Screen name="preference" component={Preference} />
                  <Stack.Screen name="settings" component={Settings} />
                </>
          }
        </Stack.Navigator>
      </NavigationContainer>
    </UserContext.Provider>
  )
}

export default App

