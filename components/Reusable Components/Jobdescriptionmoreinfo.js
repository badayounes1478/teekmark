import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const Jobdescriptionmoreinfo = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <View>
                    <Text style={styles.subContainerTitle}>Job type</Text>
                    <Text style={styles.subContainerSubTitle}>Full Time</Text>
                </View>
                <View>
                    <Text style={styles.subContainerTitle}>Joining Status</Text>
                    <Text style={styles.subContainerSubTitle}>Immediate</Text>
                </View>
                <View>
                    <Text style={styles.subContainerTitle}>Openings</Text>
                    <Text style={styles.subContainerSubTitle}>{props.data.openings}</Text>
                </View>
            </View>
            <View style={styles.border}>
                <Text style={styles.title}>Category/Sub Category</Text>
                <Text style={styles.subTitle}>{props.data.category_id.category}-{props.data.sub_category_id === null ? "" : props.data.sub_category_id.subcategory}</Text>
            </View>
            <View style={styles.border}>
                <Text style={styles.title}>Job Benefits</Text>
                <Text style={styles.subTitle}>{props.data.benefits}</Text>
            </View>
            <View style={styles.border}>
                <Text style={styles.title}>Skills</Text>
                <View style={styles.tagContainer}>
                    {
                        props.data.skills_id.map(data => {
                            return <TouchableOpacity key={data._id} style={styles.tagWrapper}>
                                <Text style={{ fontSize: 12 }}>{data.skill}</Text>
                            </TouchableOpacity>
                        })
                    }
                </View>
            </View>
        </View>
    )
}

export default Jobdescriptionmoreinfo

const styles = StyleSheet.create({
    container: {
        padding: 10,
        marginTop: 10
    },
    title: {
        color: '#919191',
        fontFamily: 'Roboto-Regular',
    },
    subTitle: {
        fontFamily: 'Roboto-Regular',
    },
    map: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#dcdcdc',
        padding: 50,
        marginTop: 20
    },
    border: {
        borderColor: '#00000038',
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        marginTop: 10
    },
    subContainer: {
        display: 'flex',
        flexDirection: 'row',
        borderColor: '#00000038',
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        justifyContent: 'space-between'
    },
    subContainerTitle: {
        color: '#919191'
    },
    tagContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10,
        width: '100%',
    },
    tagWrapper: {
        backgroundColor: '#F4F6F7',
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 5,
        marginRight: 10,
        marginBottom: 10
    }
})
