import React from 'react'
import { StyleSheet, Text, View, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native'
import Checkcolor from '../../assets/icons/Checkcolor.svg'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Axios from 'axios'
import Global from '../../Global'

const windowWidth = Dimensions.get('window').width;

const Applyforjobcard = (props) => {

    const applyForJob = async () => {
        try {
            if (Global.user.resume === undefined) {
                alert("Plz upload your resume")
            } else {
                const token = await AsyncStorage.getItem('token');
                const decoded = jwt_decode(token);
                const postData = {
                    user_id: decoded.id,
                    company_id: props.data.company_id._id,
                    category_id: props.data.category_id,
                    sub_category_id: props.data.sub_category_id,
                    job_id: props.data._id,
                    recruiter_id: props.data.recruiter_id._id
                };

                let axiosConfig = {
                    headers: {
                        'Content-Type': 'application/json',
                        "x-access-token": token
                    }
                };

                const response = await Axios.post(constant.url + "appliedjobs/applyjob", postData, axiosConfig)
                if (response.data.result === true) {
                    props.showSuccess()
                }
            }
        } catch (error) {
            alert("Something went wrong")
            console.log(error)
        }
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.closeButton} onPress={props.close}>
                <Text style={{ fontSize: 18, color: '#fff' }}>x</Text>
            </TouchableOpacity>
            <ScrollView>
                <View style={{ borderBottomColor: '#DCDCDC', borderBottomWidth: 1, paddingBottom: 10 }}>
                    <Text>Confirm Details to apply</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <View style={styles.logoBorder} >
                            <Image source={{ uri: props.data.company_id.company_logo }} style={{ width: 25, height: 25 }} />
                        </View>
                        <View style={{ marginLeft: 20 }}>
                            <Text style={styles.title}>{props.data.company_id.company_name}</Text>
                            <Text style={styles.subTitle}>{props.data.job_profile}</Text>
                        </View>
                    </View>
                </View>

                <View style={{ marginTop: 10 }}>
                    <Text style={styles.subTitle}>Contact Info</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Image style={{ height: 45, width: 45, borderRadius: 45 }} source={{ uri: Global.user.profile_photo }} />
                        <View style={{ marginLeft: 20 }}>
                            <Text style={styles.title}>{Global.user.first_name}</Text>
                            <Text>{Global.user.permenant_address}</Text>
                        </View>
                    </View>
                </View>

                <View style={{ marginTop: 10 }}>
                    <Text style={styles.subTitle}>Email</Text>
                    <Text>{Global.user.email}</Text>
                </View>

                <View style={{ marginTop: 10 }}>
                    <Text style={styles.subTitle}>Contact</Text>
                    <Text>{Global.user.mobile_no}</Text>
                </View>

                <View style={{ marginTop: 10 }}>
                    <Text style={styles.subTitle}>Resume</Text>
                    <View style={styles.border}>
                        {
                            Global.user.resume === undefined ?
                                <TouchableOpacity onPress={() => props.navigation.navigate("profile")}>
                                    <Text style={{ color: '#828282' }}>Please upload your resume</Text>
                                </TouchableOpacity> :
                                <Text style={{ width: '85%' }}>{Global.user.resume}</Text>
                        }
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ height: 35, backgroundColor: '#DBDBDB', width: 2, marginRight: 20 }}></View>
                            <Checkcolor width={30} height={30} />
                        </View>
                    </View>
                </View>

                <TouchableOpacity style={styles.button} onPress={applyForJob}>
                    <Text style={{ fontSize: 16, color: '#fff' }}>Apply</Text>
                </TouchableOpacity>

                <Text style={{ fontSize: 11, color: '#9B9B9B', marginTop: 10 }}>By applying, you agree that teekmark may share your personal information
                    with the potential employer.</Text>
            </ScrollView>
        </View>
    )
}

export default Applyforjobcard

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 15,
        position: 'absolute',
        bottom: 0,
        width: windowWidth
    },
    closeButton: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F15C20',
        borderRadius: 40,
        alignSelf: 'center',
        marginBottom: 20
    },
    logoBorder: {
        borderRadius: 100,
        borderColor: '#E9E9E9',
        borderWidth: 1,
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 16,
        fontFamily: 'Roboto-Medium'
    },
    subTitle: {
        fontFamily: 'Roboto-Regular',
        color: '#8B8B8B',
        fontSize: 16
    },
    input: {
        borderBottomColor: "#dcdcdc",
        borderBottomWidth: 1,
    },
    border: {
        borderColor: '#DBDBDB',
        borderWidth: 1,
        padding: 10,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    button: {
        backgroundColor: '#F15C20',
        alignSelf: 'center',
        paddingHorizontal: 50,
        paddingVertical: 10,
        borderRadius: 5,
        marginTop: 20
    }
})
