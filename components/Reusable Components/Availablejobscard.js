import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import secondsToDhms from '../Functions/Comparetime'

const Availablejobscard = (props) => {


    let time
    time = secondsToDhms(props.data.updatedAt)

    return (
        <TouchableOpacity style={styles.container} onPress={() => {
            props.navigation.navigate("jobdescription", {
                data:props.data
            })
        }}>
            <View style={styles.subContainer}>
                <View>
                    <Text style={styles.title}>{props.data.job_profile}</Text>
                    <Text style={styles.salary}>{props.data.ctc}</Text>
                </View>
                <Text style={styles.location}>{props.data.city_id.city}</Text>
            </View>
            <View style={styles.tagContainer}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={styles.tagWrapper}>
                        <Text style={{ fontSize: 12 }}>{props.data.jobtype_id === undefined ? "" : props.data.jobtype_id.jobtype}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tagWrapper}>
                        <Text style={{ fontSize: 12 }}>{props.data.title}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <Text style={{ fontSize: 8, position:'absolute', bottom:20, right:10 }}>{time}</Text>
        </TouchableOpacity>
    )
}

export default Availablejobscard

const styles = StyleSheet.create({
    container: {
        padding: 10,
        borderColor: '#00000038',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 10
    },
    subContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    title: {
        fontFamily: 'Roboto-Medium',
    },
    location: {
        color: '#8B8B8B',
        fontFamily: 'Roboto-Regular'
    },
    salary: {
        fontFamily: 'Roboto-Regular'
    },
    tagContainer: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between'
    },
    tagWrapper: {
        backgroundColor: '#F4F6F7',
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 5,
        marginRight: 10
    }
})
