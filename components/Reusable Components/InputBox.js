import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'

const InputBox = (props) => {
    return (
        <View style={styles.container}>
            <TextInput placeholder={props.placeholder}
                placeholderTextColor="#919191"
                secureTextEntry={props.password}
                {...props}
                style={styles.input} />
            <Text style={styles.title}>{props.title}</Text>
        </View>
    )
}

export default InputBox

const styles = StyleSheet.create({
    container: {
        height: 53,
        backgroundColor: '#F4F6F7',
        width: '100%',
        borderRadius: 10,
        alignSelf: 'center',
        position: 'relative',
        marginTop: 25,
    },
    input: {
        padding: 10,
        fontSize: 16,
        fontFamily: 'Roboto-Regular'
    },
    title: {
        position: 'absolute',
        top: -12,
        backgroundColor: '#fff',
        left: 20,
        paddingHorizontal: 10,
        borderRadius: 10
    }
})
