import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Heart from '../../assets/icons/Heart.svg'
import Like from '../../assets/icons/like.png'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Toast from 'react-native-simple-toast';
import secondsToDhms from '../Functions/Comparetime'

const Searchresultcard = (props) => {

    let time
    time = secondsToDhms(props.data.updatedAt)

    const savedTheJob = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const postData = {
                user_id: decoded.id,
                company_id: props.data.company_id._id,
                category_id: props.data.category_id._id,
                sub_category_id: props.data.sub_category_id._id,
                job_id: props.data._id,
                recruiter_id: props.data.recruiter_id._id
            };

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    "x-access-token": token
                }
            };
            const response = await Axios.post(constant.url + "shortlistedjob/shortlistjob", postData, axiosConfig)
            if (response.data.message === "Job Shortlisted Successfully") {
                Toast.show(response.data.message);
                props.likeTheJob(props.data._id)
            } else {
                Toast.show(response.data.message);
            }
        } catch (error) {
            console.log(error)
        }
    }

    const deleteTheJob = async (id) => {
        try {
            const token = await AsyncStorage.getItem('token');
            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    "x-access-token": token
                }
            };
            const response = await Axios.delete(constant.url + "shortlistedjob/removeShortlistedJob/" + id, axiosConfig)
            if (response.data.message === "Removed from Shortlisted Jobs") {
                Toast.show(response.data.message);
                props.removeTheJob(id)
            } else {
                Toast.show("In search page you can not dislike");
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <Image source={{ uri: props.uri }} style={{ width: 25, height: 25 }} />
                <TouchableOpacity style={{ marginLeft: 15 }} onPress={() => props.navigation.navigate("jobdescription", {
                    data: props.data
                })}>
                    <Text style={styles.title}>{props.companyName}</Text>
                    <Text style={styles.subTitle}>{props.jobProfile}</Text>
                    <Text style={styles.location}>{props.city}</Text>
                    <Text style={styles.salary}>{props.ctc}</Text>
                    <Text style={{ fontSize: 8 }}>{time}</Text>
                </TouchableOpacity>
            </View>
            {
                props.liked === true ?
                    <TouchableOpacity onPress={() => deleteTheJob(props.id)} style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={Like} style={{ width: 25, height: 25, marginRight: 10 }} />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={savedTheJob} style={{ height: 30, width: 60, justifyContent: 'center', alignItems: 'center', backgroundColor:'#F15C20', borderRadius:10 }}>
                        <Text style={{color:'#fff', fontSize:12}}>Save</Text>
                    </TouchableOpacity>
            }
        </View>
    )
}

export default Searchresultcard

const styles = StyleSheet.create({
    container: {
        borderColor: '#CFCFCF',
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        color: '#8B8B8B',
        fontSize: 13,
        fontFamily: 'Roboto-Regular',
    },
    subTitle: {
        fontFamily: 'Roboto-Medium',
    },
    location: {
        color: '#747474',
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    },
    salary: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
    }
})
