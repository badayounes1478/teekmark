import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Check from '../../assets/icons/Check.svg'

const Notificationcard = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <View style={styles.bar} />
                <TouchableOpacity style={styles.checkCircle}>
                    <Check height={20} width={20} />
                </TouchableOpacity>
                <View style={styles.infoContainer}>
                    <Text style={styles.title}>{props.title}</Text>
                    <Text style={styles.subTitle}>{props.description}</Text>
                </View>
            </View>
            <TouchableOpacity {...props}>
                <Text style={styles.delete}>X</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Notificationcard

const styles = StyleSheet.create({
    container: {
        borderColor: '#0000001A',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 10
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    bar: {
        backgroundColor: '#F15C20',
        height: 45,
        width: 5,
        borderRadius: 10,
        marginRight: 10
    },
    title: {
        fontFamily: 'Roboto-Medium',
    },
    subTitle: {
        fontSize: 12,
        color: '#8B8B8B',
    },
    infoContainer: {
        width: '85%',
        marginLeft: 10,
    },
    delete: {
        color: '#919191',
        marginRight: 10
    }
})
