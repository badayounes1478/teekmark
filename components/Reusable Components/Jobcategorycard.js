import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

const Jobcategorycard = (props) => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => props.filterCategory(props.id)}>
            <Text style={styles.jobTitle}>{props.category}</Text>
        </TouchableOpacity>
    )
}

export default Jobcategorycard

const styles = StyleSheet.create({
    container: {
        backgroundColor:'#20285A',
        borderRadius: 10,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        height: 100,
        width: 130
    },
    jobTitle: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginTop: 5,
        textAlign: 'center',
        color:'#fff'
    },
    availableJobs: {
        fontFamily: 'Roboto-Medium',
    }
})
