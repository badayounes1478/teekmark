import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import Trash from '../../assets/icons/trash.png'
import Edit1 from '../../assets/icons/edit1.svg'

const Addeducation = (props) => {
    return (
        <View style={styles.container}>
            <View>
                <Text style={{ fontWeight: 'bold' }}>{props.data.institute}</Text>
                <Text style={styles.fontColor}>{props.data.description}</Text>
                <Text style={styles.fontColor}>{props.data.start_year} - {props.data.end_year}</Text>
            </View>
            <TouchableOpacity style={styles.editor} onPress={() => props.removeTheEducation(props.data._id)}>
                <Image source={Trash} style={{ height: 15, width: 15 }} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.edit} onPress={() => props.navigation.navigate('education', {
                edit: true,
                user:props.data
            })}>
                <Edit1 width={15} height={15} />
            </TouchableOpacity>
        </View>
    )
}

export default Addeducation

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        marginBottom: 10,
        width: '100%',
        backgroundColor: '#fff',
        elevation: 2,
        padding: 10,
        borderRadius: 10
    },
    editor: {
        position: 'absolute',
        right: 10,
        height: 30,
        width: 30,
        top: 10,
        borderRadius: 40,
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 1
    },
    fontColor: {
        color: '#747474'
    },
    edit: {
        position: 'absolute',
        top: 10,
        right: 60,
        height: 30,
        width: 30,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 1
    }
})
