import React from 'react'
import { StyleSheet, Text, TouchableOpacity, Image, View } from 'react-native'
import Home from '../../assets/icons/Home.svg'
import Briefcase from '../../assets/icons/Briefcase.svg'
import Check from '../../assets/icons/Check.svg'
import Heart from '../../assets/icons/Heart.svg'
import BrifcaseActive from '../../assets/icons/BrifcaseActive.svg'
import Activecheck from '../../assets/icons/Activecheck.svg'
import Activeheart from '../../assets/icons/Activeheart.svg'
import Homeinactive from '../../assets/icons/home.png'
import Global from '../../Global'

const Tabbar = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.iconContainer} onPress={() => props.navigate('home')}>
                {
                    props.active === "home" ? <Home width={20} height={20} /> : <Image style={{width:20, height:20}} source={Homeinactive} />
                }
                <Text style={props.active === "home" ? { ...styles.text, color: 'black' } : styles.text}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} onPress={() => props.navigate('companies')}>
                {
                    props.active === "companies" ? <BrifcaseActive width={20} height={20} /> : <Briefcase width={20} height={20} />
                }
                <Text style={props.active === "companies" ? { ...styles.text, color: 'black' } : styles.text}>Companies</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} onPress={() => props.navigate('appliedjobs')}>
                {
                    props.active === "applied" ? <Activecheck width={20} height={20} /> : <Check width={20} height={20} />
                }
                 <Text style={props.active === "applied" ? { ...styles.text, color: 'black' } : styles.text}>Applied</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} onPress={() => props.navigate('saved')}>
                {
                    props.active === "saved" ? <Activeheart width={20} height={20} /> : <Heart width={20} height={20} />
                }
                <Text style={props.active === "saved" ? { ...styles.text, color: 'black' } : styles.text}>Saved</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconContainer} onPress={() => props.navigate('profile')}>
                <Image
                    style={styles.roundImage}
                    source={{ uri: Global.user === null ? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" : Global.user.profile_photo === undefined ? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" : Global.user.profile_photo }} />
                <Text style={props.active === "profile" ? { ...styles.text, color: 'black' } : styles.text}>Profile</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Tabbar

const styles = StyleSheet.create({
    container: {
        height: 70,
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        alignItems: 'center',
        backgroundColor: '#fff',
        right: 0,
        left: 0
    },
    text: {
        fontSize: 12,
        color: '#CFCFCF',
        fontFamily: 'Roboto-Medium',
    },
    iconContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    roundImage: {
        height: 20,
        width: 20,
        borderRadius: 20
    }
})
