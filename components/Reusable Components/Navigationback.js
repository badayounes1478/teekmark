import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const Navigationback = (props) => {
    return (
        <View style={styles.header}>
            <TouchableOpacity style={styles.backButton} onPress={props.back} >
                <Text style={{ fontSize: 20 }}>←</Text>
            </TouchableOpacity>
            <Text style={styles.headerTitle}>{props.title}</Text>
        </View>
    )
}

export default Navigationback

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
    },
    backButton: {
        width: 40,
        height: 40,
        borderRadius: 10,
        backgroundColor: '#F3F3F3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
        marginLeft: 20
    }
})
