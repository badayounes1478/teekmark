import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native'

const Jobcategorycard2 = (props) => {
    return (
        <TouchableOpacity style={styles.container} onPress={() => props.filter(props.category)} >
            <Image source={{ uri: props.url }} style={{ width: 18, height: 18 }} />
            <Text style={styles.jobTitle}>{props.category}</Text>
            <Text style={styles.availableJobs}></Text>
        </TouchableOpacity>
    )
}

export default Jobcategorycard2

const styles = StyleSheet.create({
    container: {
        borderColor: '#B2B2B2',
        borderWidth: 1,
        padding: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    jobTitle: {
        fontSize: 12,
        fontFamily: 'Roboto-Regular',
        marginTop: 5
    },
    availableJobs: {
        fontFamily: 'Roboto-Medium',
    }
})
