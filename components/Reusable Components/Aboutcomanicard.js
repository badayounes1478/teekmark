import React from 'react'
import { StyleSheet, Text, View, Linking } from 'react-native'
import Mail from '../../assets/icons/Mail.svg'
import Globe from '../../assets/icons/globe.svg'
import Facebook from '../../assets/icons/facebook-3.svg'
import Linkedin from '../../assets/icons/linkedin-icon.svg'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Aboutcompanicard = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <View>
                    <Text style={styles.subContainerTitle}>Status</Text>
                    <Text style={styles.subContainerSubTitle}>{props.status.toString()}</Text>
                </View>
                <View>
                    <Text style={styles.subContainerTitle}>Category</Text>
                    <Text style={styles.subContainerSubTitle}>Social Media</Text>
                </View>
                <View>
                    <Text style={styles.subContainerTitle}>Company type</Text>
                    <Text style={styles.subContainerSubTitle}>{props.companyType}</Text>
                </View>
            </View>
            <Text style={styles.title}>Overview</Text>
            <Text style={styles.subTitle, styles.border}>{props.overview}</Text>
            <Text style={styles.title}>Contact</Text>
            <View style={styles.border}>
                <View style={{ flexDirection: 'row' }}>
                    <Mail width={12} height={12} color="black" />
                    <Text>{props.contactNo}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Mail width={12} height={12} color="black" />
                    <Text>{props.email}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => Linking.openURL(props.website)}
                    style={{ flexDirection: 'row' }}>
                    <Globe height={12} width={12} color="black" />
                    <Text style={{color:'blue'}}>{props.website}</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.title}>Location</Text>
            <View style={styles.border}>
                <View style={{ flexDirection: 'row' }}>
                    <Globe height={12} width={12} color="black" />
                    <Text>{props.location}</Text>
                </View>
            </View>
            <Text style={styles.title}>Social Accounts</Text>
            <View style={{ flexDirection: 'row', marginTop: 10, borderColor: '#00000038', borderWidth: 1, padding: 10, borderRadius: 5 }}>
                <Facebook height={25} width={25} onPress={() => Linking.openURL(props.facebook)} />
                <Linkedin height={25} width={25}
                    onPress={() => Linking.openURL(props.linkedIn)}
                    style={{ marginLeft: 10 }} />
            </View>
        </View>
    )
}

export default Aboutcompanicard

const styles = StyleSheet.create({
    container: {
        padding: 10,
        marginTop: 10
    },
    title: {
        color: '#919191',
        marginTop: 10,
        fontFamily: 'Roboto-Regular',
    },
    subTitle: {
        fontFamily: 'Roboto-Regular',
    },
    map: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#dcdcdc',
        padding: 50,
        marginTop: 20
    },
    border: {
        borderColor: '#00000038',
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        marginTop: 10
    },
    subContainer: {
        display: 'flex',
        flexDirection: 'row',
        borderColor: '#00000038',
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        justifyContent: 'space-between'
    },
    subContainerTitle: {
        color: '#919191'
    }
})
