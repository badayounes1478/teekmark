import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from 'react-native'
import InputBox from '../Reusable Components/InputBox'
import Logo from '../../assets/icons/Logo.png'
import constant from '../../constant'
import Axios from 'axios'
import Toast from 'react-native-simple-toast';
import Loading from './Loading'

const Chnagepassword = (props) => {

    const [email, setemail] = useState(null)
    const [otp, setotp] = useState(null)
    const [password, setpassword] = useState(null)
    const [loading, setloading] = useState(false)

    useEffect(() => {
       setemail(props.route.params.email)
    }, [])

    const changePassword = async () => {
        try {
            if (email === null) {
                alert("plz enter email")
            } else if (otp === null) {
                alert("plz enter otp")
            } else if (password === null) {
                alert("plz enter password")
            } else {
                setloading(true)
                const body = {
                    email: email,
                    otp: parseInt(otp),
                    newpassword: password
                }
                const res = await Axios.post(constant.url + "User/changepasswordverifyotp", body)
                if (res.data.message === "Password changed successfully") {
                    Toast.show(res.data.message);
                    setloading(false)
                    props.navigation.navigate('login')
                } else {
                    setloading(false)
                    alert(res.data.message)
                }
            }
        } catch (error) {
            alert(error)
            setloading(false)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={styles.header}>
                                <Image source={Logo} style={styles.logo} />
                                <Text style={styles.headerTitle}>Teekmark</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputContainerTitle}>Change Password</Text>
                                <InputBox
                                    placeholder="Enter your Email address"
                                    value={email}
                                    onChangeText={(text) => setemail(text)}
                                    title="Email" />
                                <InputBox
                                    placeholder="Enter OTP"
                                    keyboardType="numeric"
                                    onChangeText={(text) => setotp(text)}
                                    title="OTP" />
                                <InputBox
                                    placeholder="Enter new password"
                                    onChangeText={(text) => setpassword(text)}
                                    secureTextEntry={true}
                                    title="Password" />
                            </View>
                            <View style={styles.bottomContainer}>
                                <TouchableOpacity style={styles.button} onPress={changePassword}>
                                    <Text style={{ color: '#fff' }}>Change Password</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        <Text style={styles.footerTitle}>{`Copyright @ 2020. TeekmarkAll\nrights reserved`}</Text>
                    </View>
            }
        </React.Fragment>
    )
}

export default Chnagepassword

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        position: 'relative'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium'
    },
    inputContainer: {
        marginTop: '4%'
    },
    inputContainerTitle: {
        fontSize: 26,
        fontFamily: 'Roboto-Bold'
    },
    inputContainerSubTitle: {
        fontSize: 18,
        fontFamily: 'Roboto-Light',
        marginVertical: 10,
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    forgot: {
        alignSelf: 'flex-end',
        fontFamily: 'Roboto-Regular'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: '40%',
        backgroundColor: '#F15C20',
        borderRadius: 10
    },
    linkContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    info: {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: '#000'
    },
    login: {
        color: '#F15C20',
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
    },
    footerTitle: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center'
    },
    logo: {
        width: 60,
        height: 45
    }
})