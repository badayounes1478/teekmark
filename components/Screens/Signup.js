import React, { useState, useEffect } from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Image, Linking } from 'react-native'
import { Picker } from '@react-native-picker/picker';
import InputBox from '../Reusable Components/InputBox'
import Logo from '../../assets/icons/Logo.png'
import Axios from 'axios'
import constant from '../../constant';
import Loading from '../Screens/Loading'
import AsyncStorage from '@react-native-async-storage/async-storage'

const Signup = (props) => {

    const [name, setName] = useState(null)
    const [email, setEmail] = useState(null)
    const [password, setPassword] = useState(null)
    const [category, setCategory] = useState([])
    const [selectedCategory, setSelectedCategory] = useState(null)
    const [selectedLocation, setselectedLocation] = useState(null)
    const [location, setlocation] = useState([])
    const [mobileNo, setmobileNo] = useState(null)
    const [profession, setProfession] = useState(null)
    const [loading, setloading] = useState(false)

    const getTheCategory = async () => {
        try {
            setloading(true)
            const response = await Axios.get(constant.url + "category/getActiveCategory")
            const city = await Axios.get(constant.url + "city/getactivecity")
            if (response.status === 200) {
                setCategory(response.data.category)
            }
            if (response.status === 200) {
                setlocation(city.data.city)
            }
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    useEffect(() => {
        getTheCategory()
    }, [])

    const signUp = async () => {
        try {
            if (name === null || email === null || password === null || mobileNo === null) return alert("Plz fill all fields")
            setloading(true)
            let fcmToken = await AsyncStorage.getItem('fcmToken')
            const response = await Axios.post(constant.url + "user/appregister", {
                name,
                email: email.trim(),
                password: password.trim(),
                person_city_id: selectedLocation,
                mobile_no: Number(mobileNo),
                category_id: selectedCategory,
                user_type: profession,
                notification_token: fcmToken
            })
            if (response.status === 200) {
                setloading(false)
                if (response.data.result !== undefined) {
                    props.navigation.navigate("verifyotp", {
                        email
                    })
                } else {
                    alert("Something went wrong")
                }
            } else {
                setloading(false)
            }
        } catch (error) {
            alert(error)
            setloading(false)
        }
    }

    return (
        <React.Fragment>
            {
                loading ? <Loading /> : <ScrollView style={styles.container}>
                    <View style={styles.header}>
                        <Image source={Logo} style={styles.logo} />
                        <Text style={styles.headerTitle}>Teekmark</Text>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.inputContainerTitle}>Hey,</Text>
                        <Text style={styles.inputContainerSubTitle}>Fill in the details to get started.</Text>
                        <InputBox placeholder="Enter your full name" title="Name" onChangeText={text => setName(text)} />
                        <InputBox
                            placeholder="Enter your Email address"
                            keyboardType="email-address"
                            autoCapitalize="none"
                            title="Email"
                            onChangeText={text => setEmail(text)} />
                        <InputBox
                            keyboardType="numeric"
                            placeholder="Enter your Mobile Number"
                            title="Mobile Number" onChangeText={text => setmobileNo(text)} />
                        <InputBox placeholder="Create Password" password={true} title="Password" onChangeText={text => setPassword(text)} />
                        <View style={styles.pickerContainer}>
                            <Picker
                                selectedValue={selectedCategory}
                                onValueChange={(itemValue) => {
                                    setSelectedCategory(itemValue)
                                }}
                            >
                                <Picker.Item color="#919191" label="Select Category" value={null} />
                                {
                                    category.map(data => {
                                        return <Picker.Item key={data._id} label={data.category} value={data._id} />
                                    })
                                }
                            </Picker>
                            <Text style={styles.title}>Category</Text>
                        </View>
                        <View style={styles.pickerContainer}>
                            <Picker
                                selectedValue={selectedLocation}
                                onValueChange={(itemValue) => {
                                    setselectedLocation(itemValue)
                                }}
                            >
                                <Picker.Item color="#919191" label="Select Location" value={null} />
                                {
                                    location.map(data => {
                                        return <Picker.Item key={data._id} label={data.city} value={data._id} />
                                    })
                                }
                            </Picker>
                            <Text style={styles.title}>Location</Text>
                        </View>
                        <View style={styles.pickerContainer}>
                            <Picker
                                selectedValue={profession}
                                onValueChange={(itemValue) => {
                                    setProfession(itemValue)
                                }}
                            >
                                <Picker.Item color="#919191" label="Select Profession" value={null} />
                                <Picker.Item label="Fresher" value="fresher" />
                                <Picker.Item label="Developer" value="developer" />
                            </Picker>
                            <Text style={styles.title}>I'm a</Text>
                        </View>
                    </View>
                    <View style={styles.bottomContainer}>
                        <TouchableOpacity style={styles.button} onPress={signUp}>
                            <Text style={{ color: '#fff' }}>Sign Up</Text>
                        </TouchableOpacity>

                        <View style={styles.linkContainer}>
                            <Text style={styles.info}>Already a member?</Text>
                            <TouchableOpacity onPress={() => props.navigation.navigate("login")}>
                                <Text style={styles.login}>Login</Text>
                            </TouchableOpacity>
                        </View>

                        <Text style={{ marginTop: 10, ...styles.footerTitle }}>By Creating an account, you are agreeing to our</Text>
                        <Text style={[styles.footerTitle, styles.footerSubTitle]}
                            onPress={() => Linking.openURL('http://google.com')}
                        >Terms of Services</Text>
                    </View>
                </ScrollView>
            }
        </React.Fragment>
    )
}

export default Signup

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: '#fff'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium'
    },
    inputContainer: {
        marginTop: '4%'
    },
    inputContainerTitle: {
        fontSize: 26,
        fontFamily: 'Roboto-Bold'
    },
    inputContainerSubTitle: {
        fontSize: 18,
        fontFamily: 'Roboto-Light'
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: '40%',
        backgroundColor: '#F15C20',
        borderRadius: 10
    },
    linkContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    info: {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: '#000'
    },
    login: {
        color: '#F15C20',
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
    },
    footerTitle: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    footerSubTitle: {
        color: '#000'
    },
    logo: {
        width: 60,
        height: 45
    },
    pickerContainer: {
        position: 'relative',
        height: 53,
        marginTop: 25,
        width: '100%',
        backgroundColor: '#F4F6F7',
        borderRadius: 10,
        alignSelf: 'center'
    },
    title: {
        position: 'absolute',
        top: -12,
        backgroundColor: '#fff',
        left: 20,
        paddingHorizontal: 10,
        borderRadius: 10
    }
})
