import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, ActivityIndicator, Image } from 'react-native'
import Axios from 'axios'
import { Picker } from '@react-native-picker/picker';
import Navigationback from '../Reusable Components/Navigationback'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Toast from 'react-native-simple-toast';
import Calender from '../../assets/icons/calendar.png'
import DateTimePicker from '@react-native-community/datetimepicker';

const Education = (props) => {

    const [degree, setdegree] = useState(null)
    const [degreeArray, setdegreeArray] = useState([])
    const [college, setcollege] = useState(null)
    const [startYear, setstartYear] = useState(null)
    const [endYear, setendYear] = useState(null)
    const [score, setscore] = useState(null)
    const [description, setdescription] = useState(null)
    const [loading, setloading] = useState(false)
    const [show, setshow] = useState(false)
    const [show1, setshow1] = useState(false)

    const getDegree = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'x-access-token': token
                }
            }
            let res = await Axios.get(constant.url + "degree/getdegree", config)
            setdegreeArray(res.data.degree)
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    useEffect(() => {
        getDegree()
        let date1 = formatDate(new Date())
        let date2 = formatDate(new Date())
        setendYear(date2)
        setstartYear(date1)
        if (props.route.params.edit === true) {
            setdegree(props.route.params.user.degree._id)
            setcollege(props.route.params.user.institute)
            setstartYear(props.route.params.user.start_year)
            setendYear(props.route.params.user.end_year)
            setscore(props.route.params.user.grade.toString())
            setdescription(props.route.params.user.description)
        }
    }, [])

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setshow(Platform.OS === 'ios');
        let date = formatDate(currentDate)
        setstartYear(date);
    };

    const onChange1 = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setshow1(Platform.OS === 'ios');
        let date = formatDate(currentDate)
        setendYear(date);
    };

    const formatDate = (date) => {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    const update = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                user_id: decoded.id,
                degree: degree,
                institute: college,
                start_year: startYear,
                end_year: endYear,
                grade: score,
                description: description,
            }

            await Axios.post(constant.url + "User/addUserEduc", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    const updateEdited = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                _id: props.route.params.user._id,
                degree: degree,
                institute: college,
                start_year: startYear,
                end_year: endYear,
                grade: score,
                description: description,
            }
            await Axios.post(constant.url + "User/updateusereducation", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }


    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <View style={styles.container}>
                        <Navigationback title="Add education" back={() => props.navigation.goBack()} />
                        <ScrollView style={{ marginTop: 20 }}>

                            <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={degree}
                                    onValueChange={(itemValue) =>
                                        setdegree(itemValue)
                                    }
                                >
                                    <Picker.Item color="#B2B2B2" label="Select Degree" value="" />
                                    {
                                        degreeArray.map(data => {
                                            return <Picker.Item
                                                key={data._id}
                                                label={data.degree}
                                                value={data._id} />
                                        })
                                    }
                                </Picker>
                                <Text style={styles.lable}>Degree</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <TextInput
                                    onChangeText={(text) => setcollege(text)}
                                    value={college}
                                    placeholder="Enter School/College Name"
                                    placeholderTextColor="#B2B2B2" />
                                <Text style={styles.lable}>School/College</Text>
                            </View>

                            <TouchableOpacity
                                onPress={() => setshow(true)}
                                style={[styles.inputContainer, { justifyContent: 'center' }]}>
                                <Text>{startYear}</Text>
                                <Image source={Calender} style={styles.calender} />
                                <Text style={styles.lable}>Start Date</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setshow1(true)}
                                style={[styles.inputContainer, { justifyContent: 'center' }]}>
                                <Text>{endYear}</Text>
                                <Image source={Calender} style={styles.calender} />
                                <Text style={styles.lable}>End Date</Text>
                            </TouchableOpacity>

                            <View style={styles.inputContainer}>
                                <TextInput
                                    onChangeText={(text) => setscore(text)}
                                    keyboardType="numeric"
                                    value={score}
                                    placeholder="Enter you grades"
                                    placeholderTextColor="#B2B2B2" />
                                <Text style={styles.lable}>Score/Grade</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <TextInput
                                    onChangeText={(text) => setdescription(text)}
                                    value={description}
                                    placeholder="Describe your qualification"
                                    placeholderTextColor="#B2B2B2" multiline={true} />
                                <Text style={styles.lable}>Description</Text>
                            </View>
                        </ScrollView>
                        {
                            props.route.params.edit === true ?
                                <TouchableOpacity style={styles.button} onPress={updateEdited}>
                                    <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                                </TouchableOpacity> :
                                <TouchableOpacity style={styles.button} onPress={update}>
                                    <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                                </TouchableOpacity>
                        }
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        )}
                        {show1 && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode={'date'}
                                is24Hour={true}
                                display="default"
                                onChange={onChange1}
                            />
                        )}
                    </View>
            }
        </React.Fragment>
    )
}

export default Education

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    inputContainer: {
        borderColor: '#DADADA',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        paddingHorizontal: 10,
        marginTop: 25,
        height: 52
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 40,
        paddingVertical: 15,
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        marginTop: 10
    },
    calender: {
        width: 26,
        height: 26,
        position: 'absolute',
        bottom: 12,
        right: 10
    }
})
