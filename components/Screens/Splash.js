import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Logo from '../../assets/icons/Logo.png'

const Splash = () => {
    return (
        <View style={styles.container}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.font}>Teekmark</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        position:'relative'
    },
    font:{
        fontFamily:'Poppins-Medium',
        color:'#919191',
        fontSize:20,
        position:'absolute',
        bottom:30
    },
    logo:{
        height:137,
        width:184
    }
})
