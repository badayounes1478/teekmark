import React, { useState, useContext, useEffect } from 'react'
import { Image, StyleSheet, View, Text, TouchableOpacity, ScrollView, ActivityIndicator, RefreshControl } from 'react-native'
import Tabbar from '../Reusable Components/Tabbar'
import Checkcolor from '../../assets/icons/Checkcolor.svg'
import Edit1 from '../../assets/icons/edit1.svg'
import Upload from '../../assets/icons/upload.svg'
import Add from '../../assets/icons/add.svg'
import { UserContext } from '../../AuthContext'
import Drawerlayout from '../Reusable Components/Drawerlayout'
import DocumentPicker from 'react-native-document-picker';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Workexperience from '../Reusable Components/Workexperience'
import Global from '../../Global'
import Addeducation from '../Reusable Components/Addeducation'

const Profile = (props) => {

    const [showDrawer, setshowDrawer] = useState(false)
    const [user, setuser] = useState(null)
    const [loading, setloading] = useState(false)
    const { signOut } = useContext(UserContext)
    const [pdfurl, setpdfurl] = useState(null)
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getUserProfile()
    }, []);

    const logOutUser = () => {
        signOut()
    }

    const getUserProfile = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                id: decoded.id,
            }
            const response = await Axios.post(constant.url + "User/getUserProfile", body, config)
            setRefreshing(false)
            setuser(response.data)
            Global.user = response.data
            setpdfurl(response.data.resume)
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }


    const selectPdf = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf, DocumentPicker.types.doc],
            });
            const file = {
                uri: response.uri,
                type: response.type,
                name: response.name,
            };
            const config = {
                headers: {
                    'content-type': 'multipart/form-data',
                    'x-access-token': token
                }
            }
            let body = new FormData();
            body.append('pdffile', file);
            let data = await Axios.post(constant.url + "user/uploadUserresume/" + decoded.id, body, config)
            setpdfurl(data.data.imageurl)
            getUserProfile()
            setloading(false)
        } catch (error) {
            console.log(error)
            setloading(false)
        }
    }

    const removeTheExperience = async (id) => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.delete(constant.url + "User/deleteUserWorkExp/" + id, {
                headers: {
                    "x-access-token": token
                }
            })
            if (response.data.message === "User Education delete successfully") {
                getUserProfile()
            } else {
                getUserProfile()
            }
        } catch (error) {
            alert(error)
        }
    }

    const removeTheEducation = async (id) => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.delete(constant.url + "User/deleteusereducation/" + id, {
                headers: {
                    "x-access-token": token
                }
            })
            if (response.data.message === "User Education delete successfully") {
                getUserProfile()
            } else {
                getUserProfile()
            }
        } catch (error) {
            alert(error)
        }
    }


    useEffect(() => {
        getUserProfile()
    }, [])

    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <React.Fragment>
                        {
                            user === null ? <ActivityIndicator size="large" color="#828282" /> :
                                <View style={styles.container}>
                                    <ScrollView
                                        refreshControl={
                                            <RefreshControl
                                                refreshing={refreshing}
                                                onRefresh={onRefresh}
                                            />
                                        }>
                                        <View style={{ height: 200, backgroundColor: 'white', position: 'relative' }}>
                                            <Image
                                                style={{ height: 200, width: '100%', resizeMode: 'contain', position: 'absolute' }}
                                                source={require('../../assets/icons/cover_image.png')} />
                                            <View style={styles.profileContainer}>
                                                <Image
                                                    style={styles.profile}
                                                    source={{ uri: user.profile_photo === undefined ? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" : user.profile_photo }}
                                                />
                                                <View style={{ ...styles.verfied }}>
                                                    <Checkcolor width={20} height={20} />
                                                </View>
                                            </View>
                                            <TouchableOpacity style={{
                                                backgroundColor: '#F3F3F3',
                                                borderRadius: 10,
                                                height: 40,
                                                width: 40,
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                alignSelf: 'flex-end',
                                                marginRight: 10,
                                                marginTop: 10
                                            }} onPress={() => setshowDrawer(true)}>
                                                <Image style={{ width: 25, height: 25 }} source={require('../../assets/icons/black_menu.png')} />
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity style={{ ...styles.edit, paddingHorizontal: 10 }} onPress={() => props.navigation.navigate("basicinformation", {
                                            user: user
                                        })}>
                                            <Edit1 width={10} height={10} />
                                            <Text style={{ color: '#F15C20', marginLeft: 10 }}>Edit</Text>
                                        </TouchableOpacity>
                                        <View style={{ marginTop: 15, alignSelf: 'center', borderBottomColor: '#20285A', borderBottomWidth: 1, width: '95%', paddingBottom: 10 }}>
                                            <Text style={styles.name}>{user.first_name}</Text>
                                            <Text style={styles.location}>{user.current_address}</Text>
                                            <Text style={styles.email}>{user.email}</Text>
                                        </View>
                                        <View style={{ marginTop: 10, paddingHorizontal: 10 }}>
                                            <View style={styles.subContainer}>
                                                <Text style={styles.title}>About</Text>
                                                <TouchableOpacity style={styles.edit} onPress={() => props.navigation.navigate('about', {
                                                    user: user
                                                })}>
                                                    <Edit1 width={10} height={10} />
                                                    <Text style={{ color: '#F15C20', marginLeft: 10 }}>Edit</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <Text>{user.about}</Text>
                                        </View>

                                        <View style={{ marginTop: 10, paddingHorizontal: 10 }}>
                                            <View style={styles.subContainer}>
                                                <Text style={styles.title}>Resume</Text>
                                                <TouchableOpacity style={{ ...styles.buttonBorder, ...styles.edit }} onPress={selectPdf}>
                                                    <Upload width={15} height={15} />
                                                    <Text style={{ color: '#F15C20', marginLeft: 10 }}>Upload</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.inputContainer}>
                                                <Text style={styles.placeHolder}>{pdfurl}</Text>
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 10, paddingHorizontal: 10 }}>
                                            <View style={styles.subContainer}>
                                                <Text style={styles.title}>Work Experience</Text>
                                                <TouchableOpacity style={{ ...styles.buttonBorder, ...styles.edit }} onPress={() => props.navigation.navigate('worksexperience', {
                                                    edit: false
                                                })}>
                                                    <Add width={15} height={15} />
                                                    <Text style={{ color: '#F15C20', marginLeft: 10 }}>Add</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.inputContainer}>
                                                {
                                                    user.workExperience.length === 0 ?
                                                        <Text style={styles.placeHolder}>Share details about your previous job</Text> :
                                                        user.workExperience.map(data => {
                                                            return <Workexperience
                                                                removeTheExperience={removeTheExperience}
                                                                key={data._id}
                                                                navigation={props.navigation}
                                                                data={data} />
                                                        })
                                                }
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 10, paddingHorizontal: 10 }}>
                                            <View style={styles.subContainer}>
                                                <Text style={styles.title}>Education</Text>
                                                <TouchableOpacity style={{ ...styles.buttonBorder, ...styles.edit }} onPress={() => props.navigation.navigate('education', {
                                                    edit: false
                                                })}>
                                                    <Add width={15} height={15} />
                                                    <Text style={{ color: '#F15C20', marginLeft: 10 }}>Add</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.inputContainer}>
                                                {
                                                    user.educationDetails.length === 0 ?
                                                        <Text style={styles.placeHolder}>Add your degrees,certificates</Text> :
                                                        user.educationDetails.map(data => {
                                                            return <Addeducation
                                                                removeTheEducation={removeTheEducation}
                                                                key={data._id}
                                                                navigation={props.navigation}
                                                                data={data} />
                                                        })
                                                }
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 10, paddingHorizontal: 10, marginBottom: 80 }}>
                                            <View style={styles.subContainer}>
                                                <Text style={styles.title}>Skills</Text>
                                                <TouchableOpacity style={{ ...styles.buttonBorder, ...styles.edit }} onPress={() => props.navigation.navigate('preference', {
                                                    user: user
                                                })}>
                                                    <Add width={15} height={15} />
                                                    <Text style={{ color: '#F15C20', marginLeft: 10 }}>Add</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.inputContainer}>
                                                {
                                                    user.skill_id === null || user.skill_id === undefined ?
                                                        <Text style={styles.placeHolder}>Add Skills</Text> :
                                                        user.skill_id.map(data => {
                                                            return <View
                                                                style={{ backgroundColor: '#F4F6F7', padding: 8, borderRadius: 10, marginRight: 10, marginBottom: 5 }}
                                                                key={data._id}>
                                                                <Text>{data.skill}</Text>
                                                            </View>
                                                        })
                                                }
                                            </View>
                                        </View>
                                    </ScrollView>
                                    {
                                        showDrawer === false ? null : <Drawerlayout navigate={props.navigation.navigate}
                                            logOutUser={logOutUser}
                                            close={() => setshowDrawer(false)} />
                                    }
                                    <Tabbar active="profile" navigate={props.navigation.navigate} />
                                </View>
                        }
                    </React.Fragment>
            }
        </React.Fragment>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    profileContainer: {
        height: 80,
        width: 80,
        borderRadius: 80,
        position: 'absolute',
        bottom: -40,
        alignSelf: 'center',
    },
    profile: {
        width: '100%',
        height: '100%',
        borderRadius: 80
    },
    verfied: {
        position: 'absolute',
        right: 0,
        bottom: 0
    },
    name: {
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
        alignSelf: 'center'
    },
    location: {
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        alignSelf: 'center',
    },
    email: {
        marginTop: 5,
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        alignSelf: 'center',
    },
    edit: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center',
        marginTop: 5
    },
    subContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10
    },
    buttonBorder: {
        borderColor: '#F3F3F3',
        borderWidth: 1,
        paddingHorizontal: 20,
        borderRadius: 10,
        paddingVertical: 8
    },
    inputContainer: {
        backgroundColor: '#fff',
        elevation: 3,
        padding: 20,
        borderRadius: 5,
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    placeHolder: {
        color: '#8B8B8B'
    },
    title: {
        fontSize: 16
    }
})
