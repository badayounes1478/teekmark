import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Dimensions, Image, Share } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Checkcolor from '../../assets/icons/Checkcolor.svg'
import Jobdescriptionmoreinfo from '../Reusable Components/Jobdescriptionmoreinfo'
import Companiescard from '../Reusable Components/Companiescard'
import Applyforjobcard from '../Reusable Components/Applyforjobcard'
import Axios from 'axios'
import constant from '../../constant'
import CheckedYellow from '../../assets/icons/checkedYellow.svg'
import AsyncStorage from '@react-native-async-storage/async-storage'
import jwt_decode from "jwt-decode";
import Toast from 'react-native-simple-toast';

const windowHeight = Dimensions.get('window').height;

const Jobdescription = (props) => {
    const [activeValue, setActiveValue] = useState(0)
    const [showCardApply, setshowCardApply] = useState(false)
    const [smiliarJobs, setsmiliarJobs] = useState([])
    const [ShowCardSucess, setShowCardSucess] = useState(false)

    const getTheSimilarJobs = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                filters: {
                    category_id: props.route.params.data.category_id._id
                },
                queryString: ""
            }
            const response = await Axios.post(constant.url + "search/searchjobs/", body, config)
            setsmiliarJobs(response.data)
        } catch (error) {
            console.log(error)
        }
    }

    const savedTheJob = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const postData = {
                user_id: decoded.id,
                company_id: props.route.params.data.company_id._id,
                category_id: props.route.params.data.category_id._id,
                sub_category_id: props.route.params.data.sub_category_id._id,
                job_id: props.route.params.data._id,
                recruiter_id: props.route.params.data.recruiter_id._id
            };

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    "x-access-token": token
                }
            };
            const response = await Axios.post(constant.url + "shortlistedjob/shortlistjob", postData, axiosConfig)
            if (response.data.message === "Job Shortlisted Successfully") {
                Toast.show(response.data.message);
            } else {
                Toast.show(response.data.message);
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getTheSimilarJobs()
        setActiveValue(0)
    }, [])

    const shareTheJob = async () => {
        try {
            const result = await Share.share({
                message: `
                Company = ${props.route.params.data.company_id.company_name}\n
                Job Profile = ${props.route.params.data.job_profile}\n
                Category = ${props.route.params.data.category_id.category}\n
                Website = ${props.route.params.data.company_id.website}\n
                Job Description = ${props.route.params.data.job_description}\n
                Recruiter Mobile No = ${props.route.params.data.recruiter_id.mobile_no}\n
                Download App = https://play.google.com/
                `,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error)
        }
    }

    const goToHome = () => {
        setShowCardSucess(false)
        props.navigation.navigate('home')
    }

    const changeScreen = (data) => {
        props.navigation.navigate("jobdescription", {
            data: data
        })
        setActiveValue(0)
    }

    return (
        <View style={showCardApply === true || ShowCardSucess === true ? { ...styles.container, backgroundColor: 'rgba(0,0,0,0.5)' } : { ...styles.container }} >
            <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 20 }}>
                <Navigationback title="Job Description" back={() => props.navigation.goBack()} />
                <View style={styles.subContainer}>
                    <View style={{ position: 'relative' }}>
                        <Image source={{ uri: props.route.params.data.company_id.company_logo }} style={{ height: 35, width: 35 }} />
                        <Checkcolor width={10} height={10} style={styles.checkIcon} />
                    </View>
                    <Text style={styles.subContainerTitle}>{props.route.params.data.job_profile}</Text>
                    <Text style={styles.subContainerSubtitleBold}>{props.route.params.data.company_id.company_name}</Text>
                    <Text style={styles.subContainerSubtitle}>{props.route.params.data.city_id.city}</Text>
                </View>

                <View style={styles.tabBar}>
                    <TouchableOpacity onPress={() => setActiveValue(0)}>
                        <Text style={activeValue === 0 ? styles.activeText : styles.tab}>Description</Text>
                        <View style={activeValue === 0 ? styles.active : null} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setActiveValue(1)}>
                        <Text style={activeValue === 1 ? styles.activeText : styles.tab}>More Info</Text>
                        <View style={activeValue === 1 ? styles.active : null} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setActiveValue(2)}>
                        <Text style={activeValue === 2 ? styles.activeText : styles.tab}>Similar jobs</Text>
                        <View style={activeValue === 2 ? styles.active : null} />
                    </TouchableOpacity>
                </View>

                <View>
                    {
                        activeValue === 0 ? <View>
                            <View style={styles.border}>
                                <View>
                                    <Text style={styles.Title}>Job Description :</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                        <View style={styles.dot} />
                                        <Text>{props.route.params.data.job_description}</Text>
                                    </View>
                                    <Text style={{ marginTop: 10, ...styles.Title }}>Qualification :</Text>
                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                        <View style={styles.dot} />
                                        <Text>{props.route.params.data.job_qualification}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.buttonContainer}>
                                <TouchableOpacity style={styles.button1}>
                                    <TouchableOpacity onPress={() => savedTheJob()}>
                                        <Text style={{ color: '#fff' }}>Save</Text>
                                    </TouchableOpacity>
                                    {
                                        /*props.route.params.data.job_shortlisted === true ?
                                            <TouchableOpacity onPress={() => Toast.show("You can't dislike job in job description screen")}>
                                                <Image source={require('../../assets/icons/like.png')} style={{ width: 25, height: 25 }} />
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={() => savedTheJob()}>
                                                <Image source={Heartoutlinecolor} style={{ width: 25, height: 25 }} />
                                            </TouchableOpacity>*/
                                    }
                                </TouchableOpacity>
                                {
                                    props.route.params.applied === undefined ? <TouchableOpacity style={styles.button2} onPress={() => setshowCardApply(true)}>
                                        <Text style={{ color: '#fff' }}>Apply for job</Text>
                                    </TouchableOpacity> :
                                        <View style={styles.button2}>
                                            <Text style={{ color: '#fff' }}>All Ready Applied</Text>
                                        </View>
                                }
                                <TouchableOpacity style={styles.button1} onPress={shareTheJob}>
                                    <Image style={{ height: 20, width: 20 }} source={require('../../assets/icons/share.png')} />
                                </TouchableOpacity>
                            </View>
                        </View> : activeValue === 1 ? <View>
                            <Jobdescriptionmoreinfo data={props.route.params.data} />
                            <View style={styles.buttonContainer}>
                                <TouchableOpacity style={styles.button1} onPress={() => savedTheJob()}>
                                    <Text style={{ color: '#fff' }}>Save</Text>
                                </TouchableOpacity>
                                {
                                    props.route.params.applied === undefined ? <TouchableOpacity style={styles.button2} onPress={() => setshowCardApply(true)}>
                                        <Text style={{ color: '#fff' }}>Apply for job</Text>
                                    </TouchableOpacity> :
                                        <View style={styles.button2}>
                                            <Text style={{ color: '#fff' }}>All ready applied</Text>
                                        </View>
                                }
                                <TouchableOpacity style={styles.button1} onPress={shareTheJob}>
                                    <Image style={{ height: 20, width: 20 }} source={require('../../assets/icons/share.png')} />
                                </TouchableOpacity>
                            </View>
                        </View> : <View>
                                    {
                                        smiliarJobs.map(data => {
                                            return <Companiescard
                                                onPress={() => changeScreen(data)}
                                                companyName={data.company_id.company_name}
                                                location={data.city_id.city}
                                                opening={data.openings}
                                                navigation={props.navigation}
                                                url={data.company_id.company_logo}
                                                data={data}
                                                updatedAt={data.updatedAt}
                                                duration={data.duration}
                                                jobtype={data.jobtype_id === undefined ? "NA" : data.jobtype_id.jobtype}
                                                key={data._id} />
                                        })
                                    }
                                </View>
                    }
                </View>
            </ScrollView>
            {
                showCardApply === true ? <Applyforjobcard data={props.route.params.data}
                    navigation={props.navigation}
                    close={() => setshowCardApply(false)} showSuccess={() => {
                        setshowCardApply(false)
                        setShowCardSucess(true)
                    }} /> : null
            }
            {
                ShowCardSucess === true ?
                    <View style={styles.cardSuccess}>
                        <CheckedYellow width={50} height={50} color="black" />
                        <Text style={styles.title}>Application Submitted Succesfully</Text>
                        <Text style={styles.subTitle}>We will keep you updated about your application.</Text>
                        <TouchableOpacity style={styles.closeButton} onPress={() => goToHome()}>
                            <Text style={{ fontSize: 18, color: '#fff' }}>Ok</Text>
                        </TouchableOpacity>
                    </View> :
                    null
            }
        </View>
    )
}

export default Jobdescription

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 10
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '10%'
    },
    subContainerTitle: {
        fontFamily: 'Roboto-Medium',
        fontSize: 18,
        marginTop: 5
    },
    subContainerSubtitle: {
        color: '#8B8B8B',
        fontSize: 12,
        marginTop: 5
    },
    subContainerSubtitleBold: {
        color: '#000',
        fontSize: 16,
        marginTop: 5,
        fontFamily: 'Roboto-Medium',
    },
    checkIcon: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    tabBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: '10%',
        marginTop: 20
    },
    tab: {
        color: '#919191'
    },
    active: {
        borderColor: '#F15C20',
        borderWidth: 3,
        borderRadius: 10,
        marginTop: 3
    },
    activeText: {
        color: '#000',
        fontFamily: 'Roboto-Medium'
    },
    border: {
        borderColor: '#00000038',
        borderWidth: 1,
        padding: 10,
        borderRadius: 5,
        marginTop: 20
    },
    Title: {
        fontFamily: 'Roboto-Medium'
    },
    dot: {
        height: 10,
        width: 10,
        backgroundColor: '#B2B2B2',
        borderRadius: 25,
        marginTop: 6,
        marginRight: 10
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        justifyContent: 'space-between',
        marginHorizontal: 10
    },
    button1: {
        borderColor: '#F15C20',
        borderRadius: 10,
        paddingVertical: 14,
        paddingHorizontal: 20,
        borderWidth: 1,
        backgroundColor: '#F15C20'
    },
    button2: {
        backgroundColor: '#F15C20',
        width: '55%',
        paddingVertical: 15,
        borderRadius: 10,
        alignItems: 'center'
    },
    cardSuccess: {
        position: 'absolute',
        backgroundColor: "#fff",
        width: '100%',
        alignSelf: 'center',
        top: windowHeight / 3,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30,
        borderRadius: 5
    },
    title: {
        fontSize: 16,
        fontFamily: 'Roboto-Medium',
        marginTop: 10,
        textAlign: 'center'
    },
    subTitle: {
        color: '#919191',
        textAlign: 'center',
        marginTop: 10
    },
    closeButton: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F15C20',
        alignSelf: 'center',
        marginTop: 20,
        paddingHorizontal: 25,
        paddingVertical: 5,
        borderRadius: 10
    }
})