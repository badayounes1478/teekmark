import React, { useEffect, useState, useContext } from 'react'
import { ScrollView, StyleSheet, View, ActivityIndicator, Image, TouchableOpacity } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Searchresultcard from '../Reusable Components/Searchresultcard'
import Tabbar from '../Reusable Components/Tabbar'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Drawerlayout from '../Reusable Components/Drawerlayout'
import { UserContext } from '../../AuthContext'

const Saved = (props) => {

    const [savedJobs, setsavedJobs] = useState([])
    const [loading, setloading] = useState(false)
    const [showDrawer, setshowDrawer] = useState(false)
    const { signOut } = useContext(UserContext)

    const logOutUser = () => {
        signOut()
    }


    const getSavedJobs = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const response = await Axios.get(constant.url + "shortlistedjob/usershortlistedjobs/" + decoded.id, {
                headers: {
                    "x-access-token": token
                }
            })
            setsavedJobs(response.data.jobs)
            setloading(false)
        } catch (error) {
            setloading(false)
            alert(error)
        }
    }

    useEffect(() => {
        getSavedJobs()
    }, [])

    const removeTheJob = (id) => {
        const filterJob = savedJobs.filter(data => data._id !== id)
        setsavedJobs(filterJob)
    }

    return (
        <React.Fragment>
            {
                loading === true ? <View style={styles.center}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> : <View style={styles.container}>
                        <Navigationback title="Saved" back={() => props.navigation.goBack()} />
                        <TouchableOpacity
                            onPress={() => setshowDrawer(true)}
                            style={styles.drawerbutton}>
                            <Image
                                style={{ height: 25, width: 25 }}
                                source={require('../../assets/icons/black_menu.png')} />
                        </TouchableOpacity>
                        {
                            showDrawer === false ? null : <Drawerlayout navigate={props.navigation.navigate}
                                logOutUser={logOutUser}
                                close={() => setshowDrawer(false)} />
                        }
                        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ marginTop: 20 }}>
                            {
                                savedJobs.map(data => {
                                    const data1 = {
                                        _id: data.job_id._id,
                                        job_profile: data.job_id.job_profile,
                                        company_id: data.company_id,
                                        recruiter_id: data.recruiter_id,
                                        category_id: data.job_id.category_id,
                                        sub_category_id: data.job_id.sub_category_id,
                                        status: data.job_id.status,
                                        designation_id: data.job_id.designation_id,
                                        skills_id: data.job_id.skills_id,
                                        title: data.job_id.title,
                                        ctc: data.job_id.ctc,
                                        city_id: data.job_id.city_id,
                                        openings: data.job_id.openings,
                                        benefits: data.job_id.benefits,
                                        duration: data.job_id.duration,
                                        payment_type: data.job_id.payment_type,
                                        hiring_type: data.job_id.hiring_type,
                                        hiring_for_company: data.job_id.hiring_for_company,
                                        job_description: data.job_id.job_description,
                                        job_qualification: data.job_id.job_qualification,
                                        jobtype_id: data.job_id.jobtype_id,
                                        updatedAt: data.updatedAt
                                    }
                                    return <Searchresultcard
                                        data={data1}
                                        liked={true}
                                        navigation={props.navigation}
                                        uri={data.company_id.company_logo}
                                        companyName={data.company_id.company_name}
                                        jobProfile={data.job_id.job_profile}
                                        city={data.job_id.city_id.city}
                                        ctc={data.job_id.ctc}
                                        id={data._id}
                                        removeTheJob={removeTheJob}
                                        key={data._id} />
                                })
                            }
                            <View style={{ marginTop: 90 }} />
                        </ScrollView>
                        <Tabbar active="saved" navigate={props.navigation.navigate} />
                    </View>
            }
        </React.Fragment>
    )
}

export default Saved

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        backgroundColor: '#fff'
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    drawerbutton: {
        position: 'absolute',
        right: 10,
        top: 20,
        height: 40,
        width: 40,
        backgroundColor: '#F3F3F3',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
