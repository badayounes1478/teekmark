import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import { Picker } from '@react-native-picker/picker';
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Global from '../../Global'
import Toast from 'react-native-simple-toast';

const Preference = (props) => {

    const [category, setcategory] = useState([])
    const [subCategory, setsubCategory] = useState([])
    const [skills, setskills] = useState([])
    const [jobLocation, setjobLocation] = useState([])
    const [loading, setloading] = useState(false)
    const [categoryId, setcategoryId] = useState(null)
    const [subCategoryId, setsubCategoryId] = useState(null)
    const [jobLocationId, setjobLocationId] = useState(null)
    const [selectedskills, setselectedskills] = useState([])

    const getCategory = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "category/getActiveCategory", {
                headers: {
                    "x-access-token": token
                }
            })
            setcategory(response.data.category)
        } catch (error) {
            console.log(error)
        }
    }

    const getSubCategory = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "category/getActivesubCategory/5fdc5d16d4bcfb3a388307c6", {
                headers: {
                    "x-access-token": token
                }
            })
            setsubCategory(response.data.category)
        } catch (error) {
            console.log(error)
        }
    }

    const getSkills = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "skill/getskill", {
                headers: {
                    "x-access-token": token
                }
            })
            setskills(response.data.skill)
        } catch (error) {
            console.log(error)
        }
    }

    const getLocation = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const response = await Axios.get(constant.url + "city/getactivecity", {
                headers: {
                    "x-access-token": token
                }
            })
            setjobLocation(response.data.city)
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    const update = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }

            const body = {
                category_id: categoryId,
                subcategory_id: subCategoryId,
                skill_id: selectedskills,
                job_location: jobLocationId,
                _id: decoded.id,
            }

            await Axios.post(constant.url + "User/editUser", body, config)
            Toast.show("pull to refresh page");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getCategory()
        getSubCategory()
        getSkills()
        getLocation()
        if (Global.user.category_id !== undefined) {
            setcategoryId(Global.user.category_id._id)
        }
        if (Global.user.subcategory_id !== undefined) {
            setsubCategoryId(Global.user.subcategory_id._id)
        }
        if (Global.user.skill_id !== undefined) {
            setselectedskills(Global.user.skill_id)
        }
        if (Global.user.job_location !== undefined) {
            setjobLocationId(Global.user.job_location._id)
        }
    }, [])

    const getTheSelectedObject = (item) => {
        let data = selectedskills.filter(data => data._id === item)
        if(data.length === 0){
            const filterValue = skills.filter(data => data._id === item)
            setselectedskills([...filterValue, ...selectedskills])
        }else{
            Toast.show("This skill is allready added");
        }
    }

    const removeSelected = (item) => {
        const filterValue = selectedskills.filter(data => data._id !== item)
        setselectedskills(filterValue)
    }

    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <View style={styles.container}>
                        <Navigationback title="Add prefrences" back={() => props.navigation.goBack()} />
                        <ScrollView>
                            <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={categoryId}
                                    onValueChange={(itemValue) =>
                                        setcategoryId(itemValue)
                                    }
                                >
                                    <Picker.Item color="#B2B2B2" label="Select Category" value="" />
                                    {
                                        category.map(data => {
                                            return <Picker.Item
                                                key={data._id}
                                                label={data.category}
                                                value={data._id} />
                                        })
                                    }
                                </Picker>
                                <Text style={styles.lable}>Category</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={subCategoryId}
                                    onValueChange={(itemValue) =>
                                        setsubCategoryId(itemValue)
                                    }
                                >
                                    <Picker.Item color="#B2B2B2" label="Select SubCategory" value="" />
                                    {
                                        subCategory.map(data => {
                                            return <Picker.Item
                                                key={data._id}
                                                label={data.subcategory}
                                                value={data._id} />
                                        })
                                    }
                                </Picker>
                                <Text style={styles.lable}>Sub Category</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <Picker
                                    onValueChange={(itemValue) =>
                                        getTheSelectedObject(itemValue)
                                    }
                                >
                                    <Picker.Item color="#B2B2B2" label="Select Skills" value="" />
                                    {
                                        skills.map(data => {
                                            return <Picker.Item
                                                key={data._id}
                                                label={data.skill}
                                                value={data._id} />
                                        })
                                    }
                                </Picker>
                                <Text style={styles.lable}>Skills</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 20, flexWrap: 'wrap' }}>
                                {
                                    selectedskills.map(data => {
                                        return <View key={data._id} style={styles.tagWrapper}>
                                            <Text>{data.skill}</Text>
                                            <TouchableOpacity style={styles.delete} onPress={() => removeSelected(data._id)}>
                                                <Text>X</Text>
                                            </TouchableOpacity>
                                        </View>
                                    })
                                }
                            </View>
                            <View style={styles.inputContainer}>
                                <Picker
                                    selectedValue={jobLocationId}
                                    onValueChange={(itemValue) =>
                                        setjobLocationId(itemValue)
                                    }
                                >
                                    <Picker.Item color="#B2B2B2" label="Select Preferred Job Location" value="" />
                                    {
                                        jobLocation.map(data => {
                                            return <Picker.Item
                                                key={data._id}
                                                label={data.city}
                                                value={data._id} />
                                        })
                                    }
                                </Picker>
                                <Text style={styles.lable}>Preferred job location</Text>
                            </View>
                        </ScrollView>
                        <TouchableOpacity style={styles.button} onPress={update}>
                            <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                        </TouchableOpacity>
                    </View>
            }
        </React.Fragment>
    )
}

export default Preference

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    inputContainer: {
        borderColor: '#DADADA',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        paddingHorizontal: 10,
        marginTop: 25
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 40,
        paddingVertical: 15,
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        marginTop: 10
    },
    tagWrapper: {
        backgroundColor: '#F4F6F7',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 5,
        marginRight: 20,
        marginBottom: 15
    },
    delete: {
        position: 'absolute',
        top: -15,
        right: 0,
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
