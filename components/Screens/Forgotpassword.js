import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import InputBox from '../Reusable Components/InputBox'
import Logo from '../../assets/icons/Logo.png'
import constant from '../../constant'
import Axios from 'axios'
import Loading from './Loading'

const Forgotpassword = (props) => {

    const [email, setemail] = useState(null)
    const [loading, setloading] = useState(false)

    const sendOTP = async () => {
        try {
            if (email === null) {
                alert("plz enter your email address")
            } else {
                setloading(true)
                const res = await Axios.post(constant.url + "User/changepasswordotp/", {
                    email: email.trim()
                })
                if (res.data.message === "OTP sent to your mobile number.") {
                    setloading(false)
                    props.navigation.navigate('changepassword', {
                        email: email
                    })
                }
            }
        } catch (error) {
            setloading(false)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <Image source={Logo} style={styles.logo} />
                            <Text style={styles.headerTitle}>Teekmark</Text>
                        </View>
                        <View style={styles.inputContainer}>
                            <Text style={styles.inputContainerTitle}>Forget Password</Text>
                            <Text style={styles.inputContainerSubTitle}>Enter your Email address registered with us,We will send you a OTP on your registered mobile number.</Text>
                            <InputBox
                                placeholder="Enter your Email address"
                                onChangeText={(text) => setemail(text)}
                                title="Email" />
                        </View>
                        <View style={styles.bottomContainer}>
                            <TouchableOpacity style={styles.button}
                                onPress={sendOTP}>
                                <Text style={{ color: '#fff' }}>Send OTP</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.footerTitle}>{`Copyright @ 2020. TeekmarkAll\nrights reserved`}</Text>
                    </View>
            }
        </React.Fragment>
    )
}

export default Forgotpassword

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        position: 'relative'
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 10
    },
    headerTitle: {
        fontSize: 20,
        fontFamily: 'Poppins-Medium'
    },
    inputContainer: {
        marginTop: '4%'
    },
    inputContainerTitle: {
        fontSize: 26,
        fontFamily: 'Roboto-Bold'
    },
    inputContainerSubTitle: {
        fontSize: 18,
        fontFamily: 'Roboto-Light',
        marginVertical: 10,
    },
    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    forgot: {
        alignSelf: 'flex-end',
        fontFamily: 'Roboto-Regular'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        width: '40%',
        backgroundColor: '#F15C20',
        borderRadius: 10
    },
    linkContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    info: {
        fontSize: 16,
        fontFamily: "Roboto-Medium",
        color: '#000'
    },
    login: {
        color: '#F15C20',
        marginLeft: 10,
        fontFamily: "Roboto-Medium",
    },
    footerTitle: {
        color: '#919191',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        alignSelf: 'center'
    },
    logo: {
        width: 60,
        height: 45
    }
})