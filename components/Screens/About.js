import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import Toast from 'react-native-simple-toast';

const About = (props) => {

    const [about, setabout] = useState("")
    const [loading, setloading] = useState(false)

    useEffect(() => {
        if (props.route.params.user === undefined) return
        setabout(props.route.params.user.about)
    }, [])

    const update = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const decoded = jwt_decode(token);
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                about: about,
                _id: decoded.id,
            }
            await Axios.post(constant.url + "User/editUser", body, config)
            Toast.show("pull the page to refresh");
            props.navigation.goBack()
            setloading(false)
        } catch (error) {
            console.log(error)
            setloading(false)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#828282" />
                </View> :
                    <View style={styles.container}>
                        <Navigationback title="About" back={() => props.navigation.goBack()} />
                        <View style={{ ...styles.inputContainer, marginTop: '10%' }}>
                            <TextInput
                                multiline={true}
                                placeholder="Write about you"
                                onChangeText={(text) => setabout(text)}
                                maxLength = {250}
                                value={about} />
                            <Text style={styles.lable}>Summary</Text>
                        </View>
                        <TouchableOpacity style={styles.button} onPress={update} >
                            <Text style={{ color: '#FFFFFF', fontSize: 16 }}>Update</Text>
                        </TouchableOpacity>
                    </View>
            }
        </React.Fragment>
    )
}

export default About

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        position: 'relative'
    },
    inputContainer: {
        borderColor: '#DADADA',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        padding: 10,
        marginTop: 25
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        alignSelf: 'center',
        backgroundColor: '#F15C20',
        width: '40%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 10,
        position: 'absolute',
        bottom: 50
    }
})
