import React, { useState } from 'react'
import { StyleSheet, Text, View, Switch, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import Navigationback from '../Reusable Components/Navigationback'
import Axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import constant from '../../constant'
import Toast from 'react-native-simple-toast';
import Loading from '../Screens/Loading'

const Settings = (props) => {

    const [isEnabled, setIsEnabled] = useState(false);
    const [open, setopen] = useState(true)
    const [currentPassword, setcurrentPassword] = useState(null)
    const [newPassword, setnewPassword] = useState(null)
    const [conformPassword, setconformPassword] = useState(null)
    const [loading, setloading] = useState(false)
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const changePassword = async () => {
        try {
            setloading(true)
            const token = await AsyncStorage.getItem('token');
            const config = {
                headers: {
                    'content-type': 'application/json',
                    'x-access-token': token
                }
            }
            const body = {
                password: currentPassword,
                newpassword: newPassword,
            }

            if (currentPassword === null) {
                alert("Plz enter current password")
            } else if (newPassword === null) {
                alert("Plz enter new password")
            } else if (conformPassword === null) {
                alert("Plz enter conform password")
            } else if (conformPassword !== newPassword) {
                alert("New password and conform password wont't match")
            } else {
                const res = await Axios.post(constant.url + "user/changePassFromApp", body, config)
                if (res.data.message === "Password changed successfully") {
                    Toast.show(res.data.message);
                }
                setloading(false)
            }
        } catch (error) {
            setloading(false)
            alert(error)
        }
    }

    return (
        <React.Fragment>
            {
                loading === true ? <Loading /> :
                    <View style={styles.container}>
                        <Navigationback title="Settings" back={() => props.navigation.goBack()} />
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {
                                /**
                                 * <Text style={{ fontSize: 16, marginTop: 30 }}>Notifications</Text>
                            <View style={styles.border}>
                                <Text style={{ fontSize: 16 }}>Enable</Text>
                                <Switch
                                    trackColor={{ false: "#dcdcdc", true: "#81b0ff" }}
                                    thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={toggleSwitch}
                                    value={isEnabled}
                                />
                            </View>
                            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, alignItems: 'center' }} onPress={() => setopen(!open)}>
                                <Text style={{ fontSize: 16 }}>Change Password</Text>
                                {
                                    open === false ? <Text style={{ color: '#221F1F42', marginRight: 20 }}>▼</Text> :
                                        <Text style={{ color: '#221F1F42', marginRight: 20 }}>▲</Text>
                                }
                            </TouchableOpacity>
                                 */
                            }
                            {
                                open === false ? null : <View style={{ marginTop: 10 }}>
                                    <View style={styles.inputContainer}>
                                        <TextInput placeholder="Enter Current password"
                                            onChangeText={(text) => setcurrentPassword(text)}
                                            placeholderTextColor="#B2B2B2" />
                                        <Text style={styles.lable}>Current Password</Text>
                                    </View>

                                    <View style={styles.inputContainer}>
                                        <TextInput placeholder="Enter New password"
                                            onChangeText={(text) => setnewPassword(text)}
                                            placeholderTextColor="#B2B2B2" />
                                        <Text style={styles.lable}>New Password</Text>
                                    </View>

                                    <View style={styles.inputContainer}>
                                        <TextInput placeholder="Confirm Password"
                                            onChangeText={(text) => setconformPassword(text)}
                                            placeholderTextColor="#B2B2B2" />
                                        <Text style={styles.lable}>Confirm Password</Text>
                                    </View>
                                </View>
                            }
                        </ScrollView>
                        <TouchableOpacity style={styles.button} onPress={changePassword}>
                            <Text style={{ color: '#fff', fontSize: 16 }}>Update</Text>
                        </TouchableOpacity>
                    </View>
            }
        </React.Fragment>
    )
}

export default Settings

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    border: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderColor: '#919191',
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 15,
        borderRadius: 5,
        marginTop: 10
    },
    inputContainer: {
        borderColor: '#919191',
        borderRadius: 5,
        borderWidth: 1,
        position: 'relative',
        paddingHorizontal: 10,
        marginTop: 25
    },
    lable: {
        position: 'absolute',
        top: -14,
        color: '#919191',
        backgroundColor: '#fff',
        left: 30,
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#F15C20',
        borderRadius: 10,
        paddingHorizontal: 40,
        paddingVertical: 15,
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 20,
        marginTop: 10
    }
})
